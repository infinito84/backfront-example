import express from 'express';
import { Init } from 'backfront';
import { Test } from './pages/test'

const app = express();

Init({
  app: app,
  port: process.env.PORT || 3000,
  pages: [
    Test
  ]
})