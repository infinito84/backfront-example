# Backfront Example

Example of use of [Backfront Framework](https://gitlab.com/infinito84/backfront)

## Running

```
npm install
npm start
```

And go to: http://localhost:3000/test

## Authors

* **Sergio Andrés Ñustes** - [infinito84](https://gitlab.com/infinito84)

## License

This project is licensed under the MIT License