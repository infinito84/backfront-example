import { Backend, Frontend } from 'backfront'

export class Test {
  route = '/test'
  title = 'Welcome'
  show = true
  list = ['Sergio', 'Javs', 'Jisel']
  list2 = ['a','b']

  template = `
    <meta charset="utf-8"/>
    <div class="{{this.title ? 'has-title' : 'no-title'}}" if="{{this.show}}">
      Hola {{this.title}}
      <span class="bc-icon-x" click="{{this.title = 'Cambié '+Math.random()}}">Click me</span>
      jejej
    </div>  
    <button click="{{this.show = !this.show}}">{{this.show ? 'Ocultar' : 'Mostrar'}}</button>
    <ul>
      <li for="this.list" click="{{this.showItem(it, $event)}}">
        {{it}}
        Names: <span for="this.list2" class="{{isNaN(it) ? 'char': 'number'}}">{{it}}</span>
      </li>
      <li>
        <button click="{{this.changeList()}}">Cambiar lista</button>
      </li>
      <li for="el in this.list2" class="{{isNaN(el) ? 'char': 'number'}}" click="{{this.showItemBackend(el)}}">{{el}}</li>
    </ul>
  `

  init(){
    /*return new Promise((resolve, reject) => {
      setTimeout(()=>{
        this.list = ['somos', 'otros', 'demorados']
        resolve()
      }, 2000)
    })*/
  }

  @Frontend
  changeList(){
    this.list2 = (100000 * Math.random()).toString(36).split('').filter(c => c !== '.')
  }

  @Frontend
  showItem(e, $event){
    console.log(e, $event)
  }

  @Backend
  showItemBackend(e){
    this.list2 = [e, 'front', 'back']
  }
}